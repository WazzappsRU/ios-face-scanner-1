//
//  Settings.swift
//  face
//
//  Created by Admin on 22.02.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

import Foundation

class Settings {
    
    class func getSettings() -> NSDictionary! {
        
        var dict: NSDictionary?
        if let path = NSBundle.mainBundle().pathForResource("Settings", ofType: "plist"){
            dict = NSDictionary(contentsOfFile: path)
        } else {
            dict = NSDictionary()
        }
        
        return dict!
        
    }    
}
